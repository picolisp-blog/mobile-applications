(local) (iceCream *Sse)

(de iceCream (Msg Osm Category Type)
   (serverSend *Sse Msg)
   (ssl "overpass-api.de"
      (pack
         "/api/interpreter?data=node++["
         Category
         "="
         Type
         "]++("
         (format
            (- (or (caar Osm) (- *Latitude 0.1)) 90.0)
            *Scl )
         ","
         (format
            (- (or (caadr Osm) (- *Longitude 0.1)) 180.0)
            *Scl )
         ","
         (format
            (- (or (cdar Osm) (+ *Latitude 0.1)) 90.0)
            *Scl )
         ","
         (format
            (- (or (cdadr Osm) (+ *Longitude 0.1)) 180.0)
            *Scl )
         ");out;" )
      (make
         (when (xml?)
            (for L (xml)
               (when (== 'node (car L))
                  (link
                     (cons
                        (attr
                           (find '((X) (= '(k . "name") (caadr X))) (cddr L))
                           'v )
                        (+ 90.0 (format (attr L 'lat) *Scl))
                        (+ 180.0 (format (attr L 'lon) *Scl)) ) ) ) ) ) ) ) )
