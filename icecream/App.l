"Mia's Ice Cream Finder" "@lib/gis.l"

(symbols 'icecream 'gis 'android 'pico)

(once
   (scl 6)
   (load "@lib/xm.l" "icecream/lib.l") )

(local) (*IceCreamOsm *Sse *Latitude *Longitude *IceList *SupermarketList)

(start
   (scl 6)
   (setq *Osm '*IceCreamOsm)
   (task -6000 1000
      (nond
         ((location?)
            (serverSend *Sse ,"Please enable \"Location\"") )
         ((gps)
            (serverSend *Sse ,"Waiting for location") )
         (NIL
            (setq *Latitude (car @)  *Longitude (cdr @))
            (serverSend *Sse (prin (lat *Latitude) ", " (lon *Longitude))) ) ) ) )

(stop
   (task -6000) )

(de work ()
   (pilbox
      (<h5> "fh" "Mia's Ice Cream Finder")
      (<h6> '((id . "icecream") "red")) )
   (menu "Mia's Ice Cream Finder"
      (serverSentEvent "icecream" '*Sse)
      (form NIL
         (<div> "bg-light container-sm border"
            (<div> "d-flex flex-column "
               (<h3> "mt-5 mb-4 d-flex justify-content-center" "Want ice cream?" )
               (<div> "mb-3 d-flex justify-content-center"
                  (<p> NIL "Find ice cream near your current location!") )
               (<div> "d-flex justify-content-center"
                  # Search for ice cafe
                  (gui '(+Style +Button) "button-icon bg-white mb-5 mx-3"
                     T "icecream/img/ice.png"
                     '(setq *IceList (iceCream ,"Fetching Ice Cafes ..." *IceCreamOsm "amenity" "ice_cream")) )
                  # Search for supermarket
                  (gui '(+Style +Button) "button-icon bg-white mb-5 mx-3"
                     T "icecream/img/supermarket.png"
                     '(setq *SupermarketList (iceCream ,"Fetching Supermarkets ..." *IceCreamOsm "shop" "supermarket")) ) )
               (<div> "map"
                  (<osm> *Latitude *Longitude 12)
                  (<poi> *Latitude *Longitude
                     "icecream/img/here.png" "0.1" "1.0"
                     "" 0 "black" )
                  (for Ice *IceList
                     (<poi> (cadr Ice) (cddr Ice)
                        "icecream/img/ice-mini.png" "0.1" "1.0" (car Ice) 0 "red") )
                  (for Market *SupermarketList
                     (<poi> (cadr Market) (cddr Market)
                        "icecream/img/supermarket-mini.png" "0.1" "1.0" (car Ice) 0 "red") ) ) ) ) ) ) )

(work)
