"Mias Ice Cream Finder (Pt. 1)" "@lib/gis.l"

(symbols 'icecream 'gis 'android 'pico)

(local) (*Latitude *Longitude)

(start
   (scl 6)
   (task -6000 1000
      (nond
         ((location?) NIL )
         ((gps) NIL )
         (NIL
            (setq *Latitude (car @)  *Longitude (cdr @)) ) ) ) )

(de work ()
   (pilbox
      (<h5> "fh" "Mia's Ice Cream Finder") )
   (menu "Mia's Ice Cream Finder"
      (form NIL
         (<div> "bg-light container-sm border"
            (<div> "d-flex flex-column "
               (<h3> "mt-5 mb-4 d-flex justify-content-center" "Want ice cream?" )
               (<div> "mb-3 d-flex justify-content-center"
                  (<p> NIL "Find ice cream near your current location!") )
               (<div> "d-flex justify-content-center"
                  # Search button for ice cafe
                  (gui '(+Style +Button) "button-icon bg-white mb-5 mx-3"
                     T "icecreamPt1/img/ice.png" NIL)
                  # Search button for supermarket
                  (gui '(+Style +Button) "button-icon bg-white mb-5 mx-3"
                     T "icecreamPt1/img/supermarket.png" NIL) ) ) ) 
               # OpenStreetMap
               (<div> "map"
                  (<osm> *Latitude *Longitude 12)
                  (<poi> *Latitude *Longitude
                     "icecreamPt1/img/here.png" "0.1" "1.0"
                     "" 0 "black" ) ) ) ) )

(work)

