"Mia's Camera App"

(symbols 'miasCamera 'android 'pico)
(local) *Picture

(start)

(setq *Picture "miasCamera/img/img.jpg")

(de work ()
   (menu "Mia's Camera App"
      (form NIL
         (<div> "bg-light container-sm border"
            (<div> "d-flex flex-column "
               (<h3> "d-flex m-5 justify-content-center" "Take a picture?")
               (<div> "d-flex m-2 justify-content-center"
                  (gui '(+Style +Able +Button) "button-icon bg-white" '(camera?) T "miasCamera/img/camera.png"
                     '(takePicture (tmp "img.jpg")
                        '((Intent)
                           (clearCache)
                           (call "cp" (tmp "img.jpg") "miasCamera/img/img.jpg")
                           (loadUrl (baseHRef) *SesId "miasCamera/App.l") ) ) ) )
               (<h5> "d-flex m-5 justify-content-center" "Your previous picture:")
               (<div> "d-flex mb-5 justify-content-center" (gui '(+Style +Var +Img) "my-img" '*Picture )) ) ) ) ) )

(work)
