"Mia's Camera App - Part 1"

(menu "Mia's Camera App, Part 1"
   (form NIL
      (gui '(+Able +Button) '(camera?) "Take picture"
         '(takePicture (tmp "img.jpg")
            '((Intent)
               (call "cp" (tmp "img.jpg") "miasCamera-Pt1/img/img.jpg")
               (loadUrl (baseHRef) *SesId "miasCamera-Pt1/App.l") ) ) ) 
    (gui '(+Var +Img) "miasCamera-Pt1/img/img.jpg") ) )

